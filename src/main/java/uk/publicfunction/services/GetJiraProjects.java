package uk.publicfunction.services;

import io.vertx.core.json.JsonArray;
import uk.publicfunction.restClient.JiraClientApi;

import java.io.IOException;

public class GetJiraProjects {
    JiraClientApi jiraClientApi;

    public GetJiraProjects() {
        this.jiraClientApi = new JiraClientApi();
    }

    public JsonArray projects() throws IOException, InterruptedException {
        String endpoint = this.jiraClientApi.getProjectsEndpoint();
        return new JsonArray(this.jiraClientApi.get(endpoint));
    }
}
