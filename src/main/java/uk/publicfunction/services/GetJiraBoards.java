package uk.publicfunction.services;

import io.vertx.core.json.JsonObject;
import uk.publicfunction.restClient.JiraClientApi;

import java.io.IOException;

public class GetJiraBoards {

    JiraClientApi jiraClientApi;

    public GetJiraBoards() {
        this.jiraClientApi = new JiraClientApi();
    }

    public JsonObject dashboards() throws IOException, InterruptedException {
        String endpoint = this.jiraClientApi.getDashboardsEndpoint();
        return new JsonObject(this.jiraClientApi.get(endpoint));
    }
    public JsonObject boards() throws IOException, InterruptedException {
        String endpoint = this.jiraClientApi.getBoardsEndpoint();
        return new JsonObject(this.jiraClientApi.getFromAgile(endpoint + "?jql=type=scrum&simple&orderBy=name"));
    }
}
