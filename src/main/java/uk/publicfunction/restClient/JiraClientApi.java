package uk.publicfunction.restClient;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;


public class JiraClientApi {

    HttpClient httpClient;

    public JiraClientApi() {
        httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build();
    }

    // Configs
    public Config getJiraConfigurations() {
        return ConfigProvider.getConfig();
    }
    public String getBaseUri() {
        return ConfigProvider.getConfig().getValue("jira.base-url", String.class);
    }
    public String getBaseUriAgile() {
        return ConfigProvider.getConfig().getValue("jira.base-url-agile", String.class);
    }
    public String getApiVersion() {
        return ConfigProvider.getConfig().getValue("jira.api-version", String.class);
    }
    public String getApiVersionAgile() {
        return ConfigProvider.getConfig().getValue("jira.api-version-agile", String.class);
    }
    public String getRequestUri() {
        return this.getApiVersion() + "/";
    }
    public String getRequestUriAgile() {
        return this.getApiVersionAgile() + "/";
    }
    public String getApiUser() {
        return ConfigProvider.getConfig().getValue("jira.api-user", String.class);
    }
    public String getApiToken() {
        return ConfigProvider.getConfig().getValue("jira.api-token", String.class);
    }

    // Endpoints
    public String getProjectsEndpoint() {
        return ConfigProvider.getConfig().getValue("jira.project-endpoint", String.class);
    }
    public String getDashboardsEndpoint() {
        return ConfigProvider.getConfig().getValue("jira.dashboards-endpoint", String.class);
    }
    public String getBoardsEndpoint() {
        return ConfigProvider.getConfig().getValue("jira.boards-endpoint", String.class);
    }
    public String getIssueTypesEndpoint() {
        return ConfigProvider.getConfig().getValue("jira.issuetypes-all-endpoint", String.class);
    }
    public String getEpicsEndpoint() {
        return ConfigProvider.getConfig().getValue("jira.issuetypes-epics", String.class);
    }



    private CredentialsProvider getCredentials() {
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(this.getApiUser(), this.getApiToken());
        provider.setCredentials(AuthScope.ANY, credentials);
        return provider;
    }

    private String getAuthBasic() {
        String toEncode = this.getApiUser() + ":" + this.getApiToken();

        return Base64.getEncoder().encodeToString(toEncode.getBytes());
    }

    public String get(String endpoint) throws IOException, InterruptedException {
        URI requestUri = URI.create(this.getBaseUri() + this.getRequestUri() + endpoint);
        HttpRequest clientRequest = HttpRequest.newBuilder()
                .uri(requestUri)
                .header("Accept", "application/json")
                .header("Authorization", "Basic " + this.getAuthBasic())
                .build();

        return this.httpClient.send(clientRequest, HttpResponse.BodyHandlers.ofString()).body();
    }

    public String getFromAgile(String endpoint) throws IOException, InterruptedException {
        URI requestUri = URI.create(this.getBaseUriAgile() + this.getRequestUriAgile() + endpoint);
        HttpRequest clientRequest = HttpRequest.newBuilder()
                .uri(requestUri)
                .header("Accept", "application/json")
                .header("Authorization", "Basic " + this.getAuthBasic())
                .build();

        return this.httpClient.send(clientRequest, HttpResponse.BodyHandlers.ofString()).body();
    }

    public void post() {
        System.out.println("POST CALLED");
    }

    public void put() {
        System.out.println("PUT CALLED");
    }

    public void delete() {
        System.out.println("DELETE CALLED");
    }
}
