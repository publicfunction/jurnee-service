package uk.publicfunction;

import io.vertx.core.json.JsonArray;
import uk.publicfunction.services.GetJiraProjects;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/projects")
public class ProjectsResource {

    private GetJiraProjects service;

    @PostConstruct
    void initialize() {
        this.service = new GetJiraProjects();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray projects() throws Exception {
        try {
            return this.service.projects();
        } catch (Exception e) {
            throw e;
        }
    }
}