package uk.publicfunction;

import io.vertx.core.json.JsonObject;
import uk.publicfunction.services.GetJiraBoards;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/dashboards")
public class DashboardResource {

    private GetJiraBoards service;

    @PostConstruct
    void initialize() {
        this.service = new GetJiraBoards();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject dashboards() throws Exception {
        try {
            return this.service.dashboards();
        } catch (Exception e) {
            throw e;
        }
    }
}