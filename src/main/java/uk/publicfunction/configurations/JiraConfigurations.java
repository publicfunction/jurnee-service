package uk.publicfunction.configurations;

import io.quarkus.arc.config.ConfigProperties;

@ConfigProperties(prefix = "jira")
public class JiraConfigurations {

    private String baseUrl;
    private String apiUser;
    private String apiToken;
    private String apiVersion;
    private String boardsEndpoint;
    private String baseUrlAgile;

    public String getBaseUrl() {
        return this.baseUrl;
    }
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getApiVersion() {
        return this.apiVersion;
    }
    public void setApiVersion(String version) {
        this.apiVersion = version;
    }

    public String getApiUser() {
        return this.apiUser;
    }
    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public String getApiToken() {
        return this.apiToken;
    }
    public void setApiToken(String token) {
        this.apiToken = token;
    }

    public String getBoardsEndpoint() {
        return this.boardsEndpoint;
    }
    public void setBoardsEndpoint(String endpoint) {
        this.boardsEndpoint = endpoint;
    }

    public String getBaseUrlAgile() {
        return this.baseUrlAgile;
    }
    public void setBaseUrlAgile(String baseUrl) {
        this.baseUrlAgile = baseUrl;
    }

}
